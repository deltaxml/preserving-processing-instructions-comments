// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelinedComparatorError;
import com.deltaxml.cores9api.ComparisonException;
import com.deltaxml.cores9api.FilterProcessingException;
import com.deltaxml.cores9api.LicenseException;
import com.deltaxml.cores9api.PipelineLoadingException;
import com.deltaxml.cores9api.PipelineSerializationException;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.cores9api.config.PreservationProcessingMode;

/**
 * A simple class for preserving processing instructions and comments, when using a 'Core S9API' pipelined comparator.
 */
public class Preserve {

  /**
   * Compare two input files and return a result file.
   */
  public static void main(String[] args) {
    if (args.length != 3) {
      System.out.println("java -cp <deltaxml.jar>:class Preserve <in1> <in2> <out>");
      System.exit(1);
    }

    try {
      // Use the base pre-defined preservation mode, which essentially provides a
      // neutral starting point, where only text, attribute and element nodes are
      // being preserved.
      LexicalPreservationConfig lpc= new LexicalPreservationConfig("base");

      // Enable comments and processing instructions to be converted into
      // elements for the purposes of comparison.
      lpc.setPreserveComments(true);
      lpc.setPreserveProcessingInstructions(true);

      // Use the 'B' version of comment and processing instructions, otherwise
      // leave all other preserved items alone.
      lpc.setDefaultProcessingMode(PreservationProcessingMode.CHANGE);
      lpc.setCommentProcessingMode(PreservationProcessingMode.B);
      lpc.setProcessingInstructionProcessingMode(PreservationProcessingMode.B);

      // Construct a pipelined comparator, then configure its lexical preservation
      // as defined above.
      PipelinedComparatorS9 pc= new PipelinedComparatorS9();
      pc.setLexicalPreservationConfig(lpc);

      // Run the comparison
      pc.compare(new File(args[0]), new File(args[1]), new File(args[2]));

    } catch (ParserInstantiationException e) {
      e.printStackTrace();
    } catch (ComparatorInstantiationException e) {
      e.printStackTrace();
    } catch (ComparisonException e) {
      e.printStackTrace();
    } catch (FilterProcessingException e) {
      e.printStackTrace();
    } catch (PipelineLoadingException e) {
      e.printStackTrace();
    } catch (PipelineSerializationException e) {
      e.printStackTrace();
    } catch (LicenseException e) {
      e.printStackTrace();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (PipelinedComparatorError e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
