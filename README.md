# Preserving Processing Instructions and Comments
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

How to convert processing instructions and comments into XML markup (and back again) so that they are not removed by XML Compare.

This document describes how to run the sample. For concept details see: [Preserving Processing Instructions and Comments](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/preserving-processing-instructions-and-comments)

## Run DXP Sample (with Ant)
If you have Ant installed, use the build script provided to run the sample, from the directory you have downloaded the sample resources to, normally samples/PreservePIsAndComments. To use the DXP configuration exploiting a lexicalPreservation element, simply type the following command to run the pipeline and produce the dxp-lp-result.xml output file.

	ant run

## Run Java API Sample
Use the following command to compile and run the sample with the API approach and produce the output file api-result.xml.

	ant run-api
	
To clean up the sample directory, run the following command in Ant.

	ant clean

## Run DXP Sample (with command-line)
If you don't have Ant installed, you can run the sample DXP from a command line by issuing the following command from your sample directory (ensuring that you use the correct directory separators for your operating system). Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	java -jar ../../deltaxml-x.y.z.jar compare preserve input1.xml input2.xml result.xml

